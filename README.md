# ACP Time Calculator

Used for calculating RUSA ACP times.

Each time a distance is filled in, the corresponding open and close times are displayed.

After ensuring that docker is installed, execute `/brevets/run.sh` to build and run the container.
Navigate to: `http://127.0.0.1:<PORT>/` to see the calculator main page

**Note:** <PORT> value is set in credentials file.

## Domain Logic

ACP time calculations are performed by the funtions located in the /brevets/acp_times.py file.

The algorithm in these functions was developed using the guidelines from the following pages:

- https://rusa.org/pages/acp-brevet-control-times-calculator
- https://rusa.org/pages/rulesForRiders
- https://rusa.org/pages/orgreg

Test cases for the functions are located in the /brevets/test_cases.py file.
Tests can be run by changing to the /brevets/ directory and typing "nosetests" in the command line.

**Note:** In the linked guidelines above, there is a special case for control points under 60KM. They say there is a special code to calculate relaxed control close times for the first 60KM. However this special code is not specified in their documentation. Because of this I have made a choice to not accept control point times of less than 60KM. Once the special code is identified it can be added. Thanks for understanding. Entering a control point of 45KM for example, will redirect the user to the invalid input screen (/brevets/templates/input_error.html).

## AJAX and Flask Implementation

The open and close times are displayed as soon as a control point distance is entered. This is done using
AJAX and Flask. 

The implementation of these can be found in the /brevets/flask_brevets.py and /brevets/templates/calc.html files.

Bad user input error checking is performed in the calc.html file.

### Author(s):

Riley Matthews, rmatthe2@uoregon.edu or riley@cs.uoregon.edu

Credits to Michal Young and Ram Durairajan for the initial version of this code.